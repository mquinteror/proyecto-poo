package proyectopoo;

import proyectopoo.view.ConnectInterface;
import proyectopoo.view.LoginInterface;
import proyectopoo.view.RegistrationInterface;

public class ClientModule{
    private ConnectInterface c_interface;
    private LoginInterface l_interface;
    private RegistrationInterface r_interface;

    public ClientModule(){
        this.c_interface = null;
        this.l_interface = null;
        this.r_interface = null;
    }
    public void startClient(){
        
        System.out.println("##############Módulo del cliente - Sistema de Venta de muebles en linea ##############");
        this.c_interface = new ConnectInterface();
        c_interface.show();
    }
}