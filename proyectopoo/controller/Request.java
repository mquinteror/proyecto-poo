package proyectopoo.controller;

import java.io.Serializable;

public class Request implements Serializable{
    private int operation;
    private String argument1;
    private String argument2;
    private Object data;
    
    public Request(){
        // default constructor
    }
    public Request( int operation, String argument1, String argument2, Object data){
        this.operation=operation;
        this.argument1=argument1;
        this.argument2=argument2;
        this.data=data;
    }
    
    public int getOperation(){
        return this.operation;
    }
    public String getArgument1(){
        return this.argument1;
    }
    
    @Override
    public String toString(){
        return "operation: " + this.operation +" argument1: " + this.argument1 
        + "argument2: " + this.argument2 + "data: " + this.data;
    }
}