package proyectopoo.controller;

import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.EOFException;
import proyectopoo.controller.Request;


public class ConnectionController extends Thread {
    final ObjectInputStream imputstream;
    final ObjectOutputStream outputstream;
    final Socket socket;
    private Request req;
    
    // Constructor
    public ConnectionController ( Socket socket, ObjectInputStream imputstream, ObjectOutputStream outputstream ){
        this.socket=socket;
        this.imputstream=imputstream;
        this.outputstream=outputstream;
        this.req = new Request();
    }

    public Request manageRequest( Request req ){
        this.req = new Request();
        return req;
    }

    
    @Override
    public void run(){
        Request recibedRequest;
        Request toSendRequest;
        
        int n =0;
        while(true){
            try{
                ///// recibir request del usuario
                try{
                    
                    recibedRequest = (Request)imputstream.readObject();
                    
                    if (recibedRequest.getOperation() == 100){
                        // means the user want to out
                        System.out.println("Client " + this.socket + "sends 100 means exit");
                        this.socket.close();
                        System.out.println("Conetion closed");
                        break;
                    }
                    else{
                        // cliente envio otra operacion
                        // si no se recibio 100 (salir), entonces
                        System.out.println("Se recibio esta respuesta");
                        System.out.println(recibedRequest.toString());
                
                        // armar una respuesta
                        toSendRequest = new Request(n, "the server response", "x", "x");
                        outputstream.writeObject(toSendRequest);
                
                        // break a proposito
                        //break;
                    }
                }catch(EOFException eofe){
                    System.out.println("EOFE <-recibedRequest = (Request)imputstream.readObject(); ");
                }
            }
            catch( IOException e ){
                e.printStackTrace();
            }
            catch(ClassNotFoundException cnfe){
                System.out.println("ClassNotFoundException,<- recibedRequest = (Request)imputstream.readObject(); ");
            }
        }
        // depues del while
        try{
            // closing resorces
            this.imputstream.close();
            this.outputstream.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}