package proyectopoo.view;

import java.util.ArrayList;
import proyectopoo.view.Item;

public class Canasta{
    ArrayList<Item> canastaItems;
    double granTotal;

    public Canasta(){
        this.canastaItems = new ArrayList<Item>();
        granTotal = 0.0;
    }

    public void addItem(Item newItem){
        this.canastaItems.add(newItem);
    }

    public void listItems(){
        for ( Item itm: this.canastaItems ){
            System.out.println(itm);
        }
    }

    public double getGranTotal(){
        for (Item itm: this.canastaItems){
            this.granTotal=itm.getTotal();
        }
        return this.granTotal;
    }

}