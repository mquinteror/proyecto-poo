package proyectopoo.view;

import proyectopoo.model.ClientesManager;
import java.util.Scanner;

import proyectopoo.model.Almacenista;
import proyectopoo.model.AlmacenistasManager;
import proyectopoo.model.Cliente;


public class RegistrationInterface{
    private int user_type;
    private String username;
    private ClientesManager clientesm;
    private AlmacenistasManager almacenistasm;
    Scanner scanner;

    public RegistrationInterface(int user_type){
        this.user_type = user_type;
        this.scanner= new Scanner(System.in);
    }

    public void show(){
        if ( this.user_type == 1 ){
            // registrar Cliente
            this.clientesm = new ClientesManager();
            System.out.print("\nIngrese username: ");
            this.username = this.scanner.nextLine();

            if (this.clientesm.checkClientUsernameExists(this.username) == true){
                System.out.println(" " + this.username + "Ya esta registrado");
            }
            else {
                // crear nuevo Cliente
                System.out.print("\nIgrese password: ");
                String password = this.scanner.nextLine();
                System.out.print("\nIngrese nombre completo: ");
                String nombrecompleto = this.scanner.nextLine();
                System.out.print("\nDirecion: ");
                String direccion = this.scanner.nextLine();
                System.out.print("\n Numero telefinico: ");
                String numerotelefonico = this.scanner.nextLine();

                Cliente newClient = new Cliente(this.username, password, nombrecompleto, direccion, numerotelefonico);
                this.clientesm.addClient(newClient);
            }
        }
        else if (this.user_type ==2) {
            // Registrar un Almacenista

            this.almacenistasm = new AlmacenistasManager();
            System.out.print("\nIngrese username: ");
            this.username = scanner.nextLine();
            System.out.print("\nIngrese password: ");
            String password = scanner.nextLine();
            
            if (almacenistasm.checkAlmacenistaUsernameExists(this.username) == true){
                System.out.println(" " + this.username + "Ya esta registrado");
            }
            else {
                // crear nuevo Almacenista
                Almacenista newAlmacenista = new Almacenista(this.username, password);
                this.almacenistasm.addAlmacenista(newAlmacenista);
            }

        }
    }
}