package proyectopoo.view;

public class Item{
    private String titulo;
    private double precio;
    private int piezas;

    public Item ( String titulo, double precio, int piezas ){
        this.titulo=titulo;
        this.precio=precio;
        this.piezas=piezas;
    }

    public double getTotal(){
        double total = 0.0;
        total = this.precio*this.piezas;

        return total;
    }

    @Override
    public String toString(){
        return " - " + this.titulo + " - " + this.precio + " - " + this.piezas + " - $" + this.getTotal();
    }
}