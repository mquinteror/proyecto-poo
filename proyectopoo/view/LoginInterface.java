package proyectopoo.view;

import java.util.Scanner;
import proyectopoo.model.ClientesManager;
import proyectopoo.model.AlmacenistasManager;
import proyectopoo.view.StoreInterface;

public class LoginInterface{
    private int user_type;
    private String username;
    private String password;
    private StoreInterface storei;
    private AlmacenInterface almaceni;
    private RegistrationInterface registrationi;

    private ClientesManager clientesm;
    private AlmacenistasManager almacenistasm;

    public void show(){
        int option = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Login as: \n");
        System.out.println("[1] - Ingresar como  <Cliente>");
        System.out.println("[2] - Ingresar como  <Almacenista>");
        System.out.println("[3] - Registrarse  como  <Cliente>");
        System.out.println("[4] - Registrarse como como  <Almacenista>");
        
        option=scanner.nextInt();
        
        /**Ingresar como Cliente */
        if (option ==1){
            this.user_type=1;
            System.out.print("Username: ");
            this.username = scanner.nextLine();
            this.username = scanner.nextLine();
            System.out.print("\npassword: ");
            this.password = scanner.nextLine();

            System.out.println("username: " + this.username + " password: " + this.password);

            this.clientesm = new ClientesManager();
            
            if ( this.clientesm.checkClientUsernameExists(this.username) == true ){
                //System.out.println(" " + this.username + " ya existe");
                if(this.clientesm.checkPasswordMatch(this.username, this.password) == true){
                    // conceder accerso
                    this.storei = new StoreInterface();
                    storei.show();
                }
                else{
                    // no coincide contraseña
                    System.out.println("[" + this.username + "] no coincide su contraseña");
                    //this.show();
                }
            }else{
                System.out.println("No se encontro el usuario");
                //this.show();
            }
        }

        /*Ingresar como Almacenista*/
        else if ( option ==2 ){
            this.user_type=2;
            System.out.print("Username: ");
            this.username = scanner.nextLine();
            this.username = scanner.nextLine();
            System.out.print("\npassword: ");
            this.password = scanner.nextLine();

            System.out.println("username: " + this.username + " password: " + this.password);

            this.almacenistasm = new AlmacenistasManager();
            
            if ( this.almacenistasm.checkAlmacenistaUsernameExists(this.username) == true ){
                // el username ya existe
                if(this.almacenistasm.checkPasswordMatch(this.username, this.password) == true){
                    // conceder accerso
                    this.almaceni = new AlmacenInterface();
                    almaceni.show();
                }
                else{
                    // no coincide contraseña
                    System.out.println("[" + this.username + "] no coincide su contraseña");
                    this.show();
                }
            }else{
                System.out.println("No se encontro el usuario");
                this.show();
            }
        }

        /**Registrarse como cliente */
        else if (option==3){
            this.user_type=1;
            System.out.println("opcion 3");
            this.registrationi = new RegistrationInterface(this.user_type);
            this.registrationi.show();
        }
        /** Registrase como almacenista */
        else if (option==4){
            this.user_type=2;
            this.registrationi = new RegistrationInterface(this.user_type);
            this.registrationi.show();
        }
    }


}