package proyectopoo.view;

import java.util.Iterator;
import java.util.Scanner;

import proyectopoo.model.Mueble;
import proyectopoo.model.MueblesManager;

public class AlmacenInterface {

    private MueblesManager mueblesm;
    private Scanner scanner;
    private Iterator<Mueble> mueblesIterator;
    String option;

    public AlmacenInterface(){
        this.mueblesm = new MueblesManager();
        this.scanner=new Scanner(System.in);
    }

    public void show(){
        System.out.println("Interfaz del almacen");
        System.out.println("[a] -> Agregar un nuevo producto");
        System.out.println("[m] -> Modificar un producto");
        
        this.option = this.scanner.nextLine();

        if ( this.option.equals("a") ){
            // agregar un producto
            System.out.print("\ntitulo: ");
            String titulo =this.scanner.nextLine();
            System.out.print("Marca: ");
            String marca = this.scanner.nextLine();
            System.out.print("Descripcion: ");
            String descripcion = this.scanner.nextLine();
            System.out.print("Precio: ");
            double precio = this.scanner.nextDouble();
            System.out.print("Inventario: ");
            int inventario = this.scanner.nextInt();

            Mueble newMueble = new Mueble (titulo, marca, descripcion, precio, inventario);
            this.mueblesm.addMueble(newMueble);

            System.out.println(newMueble.getTitulo() + " Añadido correctamente");
            
        }
        else if (this.option.equals("m")){
            //Modificar un producto
            System.out.println("Modificar producto");

        }
        
    }
}

