package proyectopoo.view;

import java.io.Console;
import java.util.Iterator;
import java.util.Scanner;

import proyectopoo.model.Mueble;
import proyectopoo.model.MueblesManager;
import proyectopoo.view.Canasta;

public class StoreInterface {
    private Canasta canasta;
    private MueblesManager mueblesm;
    private Iterator<Mueble> mueblesi;
    private String option;
    private Scanner scanner;

    public StoreInterface(){
        this.canasta = new Canasta();
        this.scanner=new Scanner(System.in);
    }

    public void show () {
        Mueble muebleactual;
        int numerodepiezas=0;
        this.option = null;
        this.mueblesm = new MueblesManager();
        this.mueblesi = mueblesm.getMueblesIterator();
        System.out.println("** Interfaz de la tienda **");
        while ( mueblesi.hasNext() ){

            muebleactual = mueblesi.next();
            muebleactual.mostrarInformacion();

            System.out.println("[c] -> Comprar");
            System.out.println("[n] -> Siguiente");
            System.out.println("[s] -> Comprar");
           
            this.option  = this.scanner.nextLine(); 
            
            if(this.option.equals("c")){
                System.out.println("Numero de piezas");
                numerodepiezas = scanner.nextInt();
                this.canasta.addItem(new Item(muebleactual.getTitulo(), muebleactual.getPrecio(), numerodepiezas));
                //scanner.close();
            }
            if(this.option.equals("n")){
                // nada
            }
            if (this.option.equals("s")){
                break;
            }
       
        }

        System.out.println("Sus productos Fueron seleccionados");
        this.canasta.listItems();
        System.out.println("GRAN TOTAL: " + this.canasta.getGranTotal());

        System.out.println("[s] -> Confirmar Compra");
        System.out.println("[n] -> Cancelar");
        this.option = this.scanner.nextLine();
        
        if (this.option.equals("s")){
            System.out.print("Gracias!!");
            System.out.print("Sus productos seran enviados a su direccion\n" +
            " en el transcurso del día\n" +
            "Recuerde que la forma de pago es contra entrega");
        }
        else if(this.option.equals("n")){
            System.out.print("Compra cancelada");
        }
        //scanner.close();
    }
}