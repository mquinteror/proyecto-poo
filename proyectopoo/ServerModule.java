package proyectopoo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServerModule{
    private int puerto;
    
    public void startServer ( ) {
        System.out.println("#-Sistema de venta de muebles en linea - Módulo del servidor#");
        this.puerto = 1234; // puerto por defecto
        //System.out.println("Escuchando en el puerto: " + puerto);
        try{
            ServerSocket ssocket = new ServerSocket(this.puerto);
            while(true){
                Socket socket =null;
                // socket recibe pticiones
                socket = ssocket.accept();
                System.out.println("Nuevo cliente conectado: " + socket);
                
            }
        }catch(IOException ioe){
            System.out.println("IOException");
            ioe.printStackTrace();
        }

        
    }
}