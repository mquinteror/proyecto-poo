package proyectopoo.model;

import proyectopoo.model.Mueble;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class MueblesManager {

    private ArrayList<Mueble> mueblesArrayList;
    
    File filechecker;
    FileInputStream fileinput;
    ObjectInputStream objectinput;

    FileOutputStream fileoutput;
    ObjectOutputStream objectoutput;

    public MueblesManager(){
        // verificar que no esté vacio el archivo
        filechecker = new File ("./info/muebles.data");
        if ( filechecker.exists() == false ){
            Mueble defaultMueble = new Mueble("default", "marca", "descripcion", 0.0, 0);
            ArrayList<Mueble> tmpArrayList = new ArrayList<Mueble>();
            tmpArrayList.add(defaultMueble);
            try{
                this.fileoutput = new FileOutputStream("./info/muebles.data");
                this.objectoutput = new ObjectOutputStream(this.fileoutput);
                this.objectoutput.writeObject(tmpArrayList);
                // cerrar flujos
                this.objectoutput.close();
                this.fileoutput.close();
            }
            catch (FileNotFoundException fnfe){
                fnfe.printStackTrace();
            }
            catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    public boolean checkMuebleTituloExists(String titulo){
        // leer arrayList de Muebles
        boolean exists = false;
        try{
            this.fileinput = new FileInputStream("./info/muebles.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.mueblesArrayList =(ArrayList<Mueble>)objectinput.readObject();
            
            this.objectinput.close();
            this.fileinput.close();

            for (Mueble m: this.mueblesArrayList){
                if(m.getTitulo().equals(titulo)){
                    exists = true;
                }
                else{
                    exists = false;
                }
            }
        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        return exists;
    }

    public int getTituloInventory( String titulo){
        int inventario = 0;
        try{
            this.fileinput = new FileInputStream("./info/muebles.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.mueblesArrayList =(ArrayList<Mueble>)objectinput.readObject();            
            this.objectinput.close();
            this.fileinput.close();
            
            int posicion=0;
            for ( posicion=0; posicion<mueblesArrayList.size(); posicion++ ){
                if( mueblesArrayList.get(posicion).getTitulo().equals(titulo) ){
                    inventario = mueblesArrayList.get(posicion).getInventario();
                }
                else{
                    inventario =0;
                }
            }
            
        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        return inventario;
    }

    public void addMueble( Mueble newMueble ){

        try{
            this.fileinput = new FileInputStream("./info/muebles.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.mueblesArrayList =(ArrayList<Mueble>)objectinput.readObject();            
            this.objectinput.close();
            this.fileinput.close();

            this.mueblesArrayList.add(newMueble);

            // escribir el nuevo arrayList
            this.fileoutput = new FileOutputStream("./info/muebles.data");
            this.objectoutput = new ObjectOutputStream(this.fileoutput);
            objectoutput.writeObject(this.mueblesArrayList);

            this.objectoutput.close();
            this.fileoutput.close();


        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
    }

    public Iterator<Mueble> getMueblesIterator(){
        Iterator<Mueble> mueblesIterator = null;


        try{
            this.fileinput = new FileInputStream("./info/muebles.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.mueblesArrayList =(ArrayList<Mueble>)objectinput.readObject();            
            this.objectinput.close();
            this.fileinput.close();

            mueblesIterator = this.mueblesArrayList.iterator();

        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }

        return mueblesIterator;
    }
}
