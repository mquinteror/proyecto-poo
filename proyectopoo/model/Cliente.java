package proyectopoo.model;

import java.io.Serializable;

public class Cliente implements Serializable{
    private String username;
    private String password;
    private String nombreCompleto;
    private String direccion;
    private String numeroTelefonico;
    private static final long serialVersionUID = -2379871860832813039L;

    public Cliente( String username, String password, String nombreCompleto, String direccion, String numeroTelefonico ){
        this.username=username;
        this.password=password;
        this.nombreCompleto=nombreCompleto;
        this.direccion=direccion;
        this.numeroTelefonico=numeroTelefonico;
    }

    @Override
    public String toString() {
        return "cliente: " +this.username + " " + this.password + " " + this.direccion;
    }

    public void setusername( String username ){
        this.username=username;
    }
    public String getUsername(){
        if ( this.username==null ){
            return "username is empty";
        }
        else{
            return this.username;
        }
    }
    public void setPassword (  String password){
        this.password = password;
    }
    public String getPassword( ){
        if (this.password == null){
            return "password is empty";
        }else{
            return this.password;
        }
    }
    public void setNombreCompleto( String nombreCompleto ){
        this.nombreCompleto=nombreCompleto;
    }
    public String getNombreCompleto(  ){
        if ( this.nombreCompleto==null ){
            return "nombreCompleto is empty";
        }
        else{
            return this.nombreCompleto;
        }
    }
    public void setDireccion( String direccion ){
        this.direccion = direccion;
    }
    public String getDireccion(){
        if ( this.direccion == null ){
            return "direccion is empty";
        }
        else{
            return this.direccion;
        }
    }
    public void setNumeroTelefonico ( String numeroTelefonico ){
        this.numeroTelefonico = numeroTelefonico;
    }
    public String getNumeroTelefonico(){
        if (this.numeroTelefonico == null ){
            return "numeroTelefonico is empty";
        }
        else{
            return this.numeroTelefonico;
        }
    }
}