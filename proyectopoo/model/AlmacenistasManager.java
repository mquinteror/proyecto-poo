package proyectopoo.model;

import proyectopoo.model.Almacenista;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class AlmacenistasManager {

    private ArrayList<Almacenista> almacenistasArrayList;
    
    File filechecker;
    FileInputStream fileinput;
    ObjectInputStream objectinput;

    FileOutputStream fileoutput;
    ObjectOutputStream objectoutput;

    public AlmacenistasManager(){
        // verificar que no esté vacio el archivo
        filechecker = new File ("./info/almacenistas.data");
        if ( filechecker.exists() == false ){
            Almacenista defaultAlmacenista = new Almacenista("default", "default");
            ArrayList<Almacenista> tmpArrayList = new ArrayList<Almacenista>();
            tmpArrayList.add(defaultAlmacenista);
            try{
                this.fileoutput = new FileOutputStream("./info/almacenistas.data");
                this.objectoutput = new ObjectOutputStream(this.fileoutput);
                this.objectoutput.writeObject(tmpArrayList);
                // cerrar flujos
                this.objectoutput.close();
                this.fileoutput.close();
            }
            catch (FileNotFoundException fnfe){
                fnfe.printStackTrace();
            }
            catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    public boolean checkAlmacenistaUsernameExists(String username){
        // leer arrayList de cliente
        boolean exists = false;
        try{
            this.fileinput = new FileInputStream("./info/almacenistas.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.almacenistasArrayList =(ArrayList<Almacenista>)objectinput.readObject();
            
            this.objectinput.close();
            this.fileinput.close();

            for (Almacenista a: this.almacenistasArrayList){
                if(a.getUsername().equals(username)){
                    exists = true;
                }
                else{
                    exists = false;
                }
            }
        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        return exists;
    }

    public boolean checkPasswordMatch( String username , String password){
        boolean match = false;
        try{
            this.fileinput = new FileInputStream("./info/almacenistas.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.almacenistasArrayList =(ArrayList<Almacenista>)objectinput.readObject();            
            this.objectinput.close();
            this.fileinput.close();

            for (Almacenista a: this.almacenistasArrayList){
                if(a.getUsername().equals(username)){
                    // see encontro el usuario
                    if ( a.getPassword().equals(password) ){
                        match = true;
                    }
                }
                else{
                    match = false;
                }
            }
        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        return match;
    }

    public void addAlmacenista( Almacenista newAlmacenista ){

        try{
            this.fileinput = new FileInputStream("./info/almacenistas.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.almacenistasArrayList =(ArrayList<Almacenista>)objectinput.readObject();            
            this.objectinput.close();
            this.fileinput.close();

            this.almacenistasArrayList.add(newAlmacenista);

            // escribir el nuevo arrayList
            this.fileoutput = new FileOutputStream("./info/almacenistas.data");
            this.objectoutput = new ObjectOutputStream(this.fileoutput);
            objectoutput.writeObject(this.almacenistasArrayList);

            this.objectoutput.close();
            this.fileoutput.close();

            System.out.println(newAlmacenista.getUsername() + "Añadido correctamente");


        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
    }
}
