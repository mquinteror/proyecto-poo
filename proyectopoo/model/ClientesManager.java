package proyectopoo.model;

import proyectopoo.model.Cliente;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class ClientesManager {

    private ArrayList<Cliente> clientesArrayList;
    
    File filechecker;
    FileInputStream fileinput;
    ObjectInputStream objectinput;

    FileOutputStream fileoutput;
    ObjectOutputStream objectoutput;

    public ClientesManager(){
        // verificar que no esté vacio el archivo
        filechecker = new File ("./info/clientes.data");
        if ( filechecker.exists() == false ){
            Cliente defaultClient = new Cliente("default", "default", "completename","telephone","direccion");
            ArrayList<Cliente> tmpArrayList = new ArrayList<Cliente>();
            tmpArrayList.add(defaultClient);
            System.out.println("Se sabe que el archivo no existe");
            try{
                this.fileoutput = new FileOutputStream("./info/clientes.data");
                this.objectoutput = new ObjectOutputStream(this.fileoutput);
                this.objectoutput.writeObject(tmpArrayList);
                // cerrar flujos
                this.objectoutput.close();
                this.fileoutput.close();
            }
            catch (FileNotFoundException fnfe){
                fnfe.printStackTrace();
            }
            catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    public boolean checkClientUsernameExists(String username){
        // leer arrayList de cliente
        boolean exists = false;
        try{
            this.fileinput = new FileInputStream("./info/clientes.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.clientesArrayList =(ArrayList<Cliente>)objectinput.readObject();
            
            this.objectinput.close();
            this.fileinput.close();

            for (Cliente c: this.clientesArrayList){
                if(c.getUsername().equals(username)){
                    exists = true;
                }
                else{
                    exists = false;
                }
            }
        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        return exists;
    }

    public boolean checkPasswordMatch( String username , String password){
        boolean match = false;
        try{
            this.fileinput = new FileInputStream("./info/clientes.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.clientesArrayList =(ArrayList<Cliente>)objectinput.readObject();            
            this.objectinput.close();
            this.fileinput.close();

            for (Cliente c: this.clientesArrayList){
                if(c.getUsername().equals(username)){
                    // see encontro el usuario
                    if ( c.getPassword().equals(password) ){
                        match = true;
                    }
                }
                else{
                    match = false;
                }
            }
        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        return match;
    }

    public void addClient( Cliente newClient ){

        try{
            this.fileinput = new FileInputStream("./info/clientes.data");
            this.objectinput = new ObjectInputStream(this.fileinput);

            this.clientesArrayList =(ArrayList<Cliente>)objectinput.readObject();            
            this.objectinput.close();
            this.fileinput.close();

            this.clientesArrayList.add(newClient);

            // escribir el nuevo arrayList
            this.fileoutput = new FileOutputStream("./info/clientes.data");
            this.objectoutput = new ObjectOutputStream(this.fileoutput);
            objectoutput.writeObject(this.clientesArrayList);

            this.objectoutput.close();
            this.fileoutput.close();

            System.out.println(newClient.getUsername() + " Añadido correctamente");


        }catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
    }
}
