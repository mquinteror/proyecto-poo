package proyectopoo.model;

import java.io.Serializable;

public class Almacenista implements Serializable{
    private String username;
    private String password;
    private static final long serialVersionUID = -7407684845904312304L;

    public Almacenista( String username, String password){
        this.username=username;
        this.password=password;
    }

    @Override
    public String toString() {
        return "Almacenista: " +this.username + " password: " + this.password;
    }


    public String getUsername(){
        if ( this.username==null ){
            return "username is empty";
        }
        else{
            return this.username;
        }
    }

    public String getPassword( ){
        if (this.password == null){
            return "password is empty";
        }else{
            return this.password;
        }
    }
 
}