package proyectopoo.model;

import java.io.Serializable;

public class Mueble implements Serializable {
    private String titulo;
    private String marca;
    private String descripcion;
    private double precio;
    private int inventario;

    // serialver
    private static final long serialVersionUID = -8198097695246174231L;
    
    public Mueble(){
        this.titulo=null;
        this.marca=null;
        this.descripcion=null;
        this.precio=0.0;
        this.inventario=0;
    }

    public Mueble ( String titulo, String marca, String descripcion, double precio, int inventario){

        this.titulo=titulo;
        this.marca=marca;
        this.descripcion=descripcion;
        this.precio=precio;
        this.inventario=inventario;
    }

    public void setTitulo(String titulo){
        this.titulo=titulo;
    }
    public String getTitulo(){
        if(this.titulo==null){
            return "titulo is empty";
        }
        else{
            return this.titulo;
        }
    }
    public void setMarca( String marca ){
        this.marca=marca;
    }
    public String getMarca(){
        if (this.marca==null){
            return "marca is empty";
        }
        else{
            return this.marca;
        }
    }
    public void setDescripcion( String descripcion ){
        this.descripcion=descripcion;
    }
    public String getDescripcion(){
        if (this.descripcion==null){
            return "descripcion is empty";
        }
        else{
            return this.descripcion;
        }
    }
    public void setPrecio(float precio){
        this.precio=precio;
    }
    public double getPrecio(){
        return this.precio;
    }
    public void setInventario( int inventario ){
        this.inventario=inventario;
    }
    public int getInventario(){
        return this.inventario;
    }

    public void mostrarInformacion(){
        System.out.println("Titulo: " + this.titulo);
        System.out.println("Marca: " + this.marca);
        System.out.println("Descripcion: ");
        System.out.println(this.descripcion);
        System.out.println("--------------------------");
        System.out.print("Precio: " + this.precio);
        System.out.println("   Inventario: " + this.inventario);
    }
}
