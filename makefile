all:
	# clases del paquete model
	javac proyectopoo/model/Cliente.java
	javac proyectopoo/model/Mueble.java
	javac proyectopoo/model/Almacenista.java
	javac proyectopoo/model/ClientesManager.java
	javac proyectopoo/model/AlmacenistasManager.java
	javac proyectopoo/model/MueblesManager.java
	#clases del paquete controller
	javac proyectopoo/controller/Request.java
	javac proyectopoo/controller/ConnectionController.java
	#clases del paquete view
	javac proyectopoo/view/RegistrationInterface.java
	javac proyectopoo/view/LoginInterface.java
	javac proyectopoo/view/ConnectInterface.java
	javac proyectopoo/view/StoreInterface.java
	javac proyectopoo/view/AlmacenInterface.java
	javac proyectopoo/view/Item.java
	javac proyectopoo/view/Canasta.java
	# clases del paquete proyectopoo
	javac proyectopoo/ServerModule.java
	javac proyectopoo/ClientModule.java
	#clases en el directorio principal
	javac ServerRunner.java
	javac ClientRunner.java
	# end#
